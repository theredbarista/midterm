Motivations
===========


Midterm Approach
----------------

We considered three constraints when deciding on how to approach the midterm assignment, namely:
(1) At the outset of the project there is a high degree of uncertainty about the project's direction and scope,
(2) The available ressources is fixed by the number of students in our study group, and
(3) The timeline for the work is fixed by the midterm deadline.

**Kanban**

Given these constraints we decided to approach the project using [Kanban](https://en.wikipedia.org/wiki/Kanban), which will allow us to adjust out backlog in tandem with the changes we introduce as the concept emerges. At the outset we agreed to:

- Lean our backlog against the suggested table of contents.
- Aim for the highest mark in each category given in the assignment instructions.
- Put each criteria in [GitLab a backlog](https://gitlab.com/cm2020-agile-software-projects/midterm/-/issues). 
- Keep the backlog sorted by priority.
- Everyone pulls from the top of the backlog.

**Git branches and review of merge requests**

Each team member works of a personal branch. [Merge requests](https://gitlab.com/cm2020-agile-software-projects/midterm/-/merge_requests), also known as pull requests, are created when work is ready for review. To associate commits, issues and merge requests we use git comments such as "Fix #number". 
The team then comments in GitLab during review and merges to the `team` branch when consensus is reached.

**Daily meeting at 17:00 CET**

While not strictly nessecary when using Kanban as a delivery method, the team has agreed to meets daily with the aim of ensuring steady progress. The duration of the daily meeting is 10-15 min. maximum to where each participant answers:

1) What did you tackle since yesterday?
2) What will you do next?
3) Are there any obstacles, and if so how can we resolve it as a team?

**Extended meetings**

Occationally, when topics require further discussion, then the team agrees on breakout calls following the daily meeting. Additional team meetings may also be scheduled when larger decisions are to be made. In this case a written agenda is distributed prior to each meeting, and all decisions are captured in the subsequent [minutes](./meetings).


Finals Approach
---------------

> TODO: describe how plan to approach the work towards the finals. see issue #13

