# Market Analysis of Social Media Applications

In this digital age, social media is an integral part of our lives. From Instagram to instant messaging applications like WhatsApp. And looking it more closely, the most used applications such as Facebook, Instagram and WhatsApp, are all under Meta. Other applications highly used such as Twitter, Reddit, Pinterest and Quora also have high influx of daily users.
But these among all of these, Meta has the most profitable company and has the most amount of daily users on it’s services. Everyone is using there applications from a layman to a business man, from social media influencer to mainstream media personnel. But high volume of users also produces high amount of data which is the responsibility of the company to protect and maintain the privacy of each and every user. 
With the rising cases of Meta’s ill ways of handling user’s data where there are alleged reports of Meta selling the data to other entities, and potential issue of hackers breaking into the servers, and stealing the data which leads to many reported cyber-crimes and increase in digital misconduct. 
All of this has created storm among people where there is a desperate need of a service which protects the data of it’s users and makes sure their privacy is maintained. This is where we come in with an idea of giving people back their personal data after they have closed their accounts. But this social media service will need tackle the tech giants such as Meta, Twitter, and Reddit etc. For which this analysis is broken down to three main sections which includes financial, user base and reputation analysis.

**1. Financial Analysis:**
Looking at Meta first, the company’s most profitable company up-to date is Facebook. Facebook generated a total estimated revenue of $117.92 billion in 2021 which was a 37% increase from 2020 in which they generated approximately $85.96 billion. Since this isn’t the only company Meta owns, while Instagram generated an estimated amount of $47.6 billion revenue in 2021 and this was 43.6% increase from 2020 in which they generated approximately $26.8 billion revenue. However WhatsApp which is a small part of Meta’s revenue only generated an estimated amount of $8.7 billion in 2021 from it’s WhatsApp business application. While other companies such as Twitter, Reddit etc is generating billions of revenue each year and increase in users as well which shows that people are not just stuck with one application but they are trying different ones for their different functionality and usage.
This above numbers shows how strong the company is and that still masses use their applications on a daily basis. Considering how the numbers increase from each year shows the increase in users every year. This will be a great hurdle to tackle and to bring huge amount of users on our platform takes time and trust on the service for the users to jump-ship. While also, the UI/UX, different functionality and the service response has to be spot on to maintain the user-base and make sure the best services are given which requires a team, capital and better business model which is profitable. 
So, the biggest challenge as of right now is to build an application which fulfills the people’s expectations, gives something new to the people, and is able to compete with today’s competition. (Below is a table showing revenue of each company in 2021).

Company	Revenue($bn) in 2021
Facebook	117.92
Instagram	47.6
Twitter	5.24
Reddit	0.17 

**2. User-base Analysis:**
Now that we have talked about the financial aspect of the competitors, let’s talk about it’s users.

Application	Active users in 2021
Facebook	1.93 billion
Instagram	1.28 billion
Twitter	396.5 million
Reddit	52 million

Looking at these numbers, we can see just how much users Meta has on both of it’s social media platform. While as of 2022, the Facebook users have increased to 1.96 billion. This means more people have started to trust the platform and they have attracted more users by rolling out new features such as Facebook reels to compete with TikTok in short-form content, and it is also worth mentioning that same reels functionality was introduced to Instagram as well which attracted alot of content creators and increased number of user-base.
But does this mean that people don’t care about their data even if Meta has violated many data protection rules and regulations? We believe that this is not the case, even if there are users who are not aware of the consequences of the data violations and leaking of data, there do exist people who do care about their data and their digital privacy. Now to further analyze our target group and to make sure we are clear on our expectations and aims we need to analyze the reputation of these companies and the how the users react to their data handling practices.

**3. Reputation Analysis:**
Looking over the past news and trends of decrease in the users of Meta. Even though as talked above that in 2022 the active users increased to 1.96 billion on Facebook, however in February of 2022, Facebook saw a decline in active users from 1.93 billion to 1.929 billion. This may seem like a small decrease fro Facebook but for us this is an opportunity and arises a question of why these people left the platform? Without speculating anything and just looking at the historical facts, Facebook has had the scandalous history of data misconduct and with new options in place people feel like moving to a more fresher service. 
Looking it even more closely, these people might even be those who care about their data. In 2021, after WhatsApp which is owned by Meta, changed their terms and conditions, saw a rapid decline in it’s active users. And not just that, the alternative apps such as Telegram saw an increase to 25 million active users and whereas Signal saw an active 7.5 million users.This was the time in the world where people chose themselves, and took a leap of leaving the big players and opting for something which is safe for them. Now with attitude of users in mind, we can easily place a solution to give people an better and digitally safer alternative. With this trend we can safely say that people don’t really put in their full trust in these tech giants and these corporations maybe the most profitable bu they certainly don’t have the best reputation. 
These trends have been on the rise since people are being more aware of their data protection rights and the importance of their digital footprint. This is the target audience we are looking for, since reputation is everything and builting that trust among people takes time so our solution will also be for these people who are slowly getting more aware and are looking for a better alternative. 
