# Summary

* [Introduction](README.md)
* [Team Submission](team/index.md)
  * [Aims/Objectives](team/aims-objectives.md)
  * [Planning](team/planning.md)
  * [Specification](team/specification.md)
  * [Scope](team/scope.md)
  * [Requirements](team/requirements.md)
  * [Literature](team/literature.md)
  * [Market Research](team/market-research.md)
  * [Motivations](team/motivations.md)
  * [Prototyping](team/prototyping.md)
  * [Assumption testing](team/assumption-testing.md)
  * [Analysis/Outcomes/Evaluation](team/analysis-outcomes-evaluation.md)
* [Minutes of Meeting](meetings/index.md)
* [Individual Reflections](individual/index.md)

